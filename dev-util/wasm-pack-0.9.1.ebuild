# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="This tool seeks to be a one-stop shop for building and working with rust- generated WebAssembly that you would like to interop with JavaScript, in the browser or with Node.js. wasm-pack helps you build rust-generated WebAssembly packages that you could publish to the npm registry, or otherwise use alongside any javascript packages in workflows that you already use, such as webpack or greenkeeper."
HOMEPAGE="https://rustwasm.github.io/wasm-pack/"
SRC_URI="https://github.com/rustwasm/wasm-pack/archive/v0.9.1.tar.gz"

LICENSE="MIT/Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-lang/rust"
RDEPEND="${DEPEND}"
BDEPEND="dev-lang/rust"

src_compile() {
	cargo build --release
}

src_install() {
	cargo install --root /
}
